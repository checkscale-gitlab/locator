FROM python:3-alpine

RUN adduser -SD -h /app locator

WORKDIR /app

COPY ./requirements*.txt /app/

RUN pip install -r /app/requirements-ci.txt \
  && rm /app/requirements*.txt

COPY . .

EXPOSE 8080

USER locator

ENTRYPOINT ["/usr/local/bin/gunicorn", \
            "-b", "0.0.0.0:8080", \
			"--error-logfile", "-", \
			"--access-logfile", "-", \
            "wsgi:app"]

